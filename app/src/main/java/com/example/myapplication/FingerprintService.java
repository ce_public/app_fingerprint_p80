package com.example.myapplication;

import android.graphics.Color;
import android.view.View;
import android.widget.Button;

import com.morpho.morphosmart.sdk.CompressionAlgorithm;
import com.morpho.morphosmart.sdk.ErrorCodes;
import com.morpho.morphosmart.sdk.LatentDetection;
import com.morpho.morphosmart.sdk.MorphoDevice;
import com.morpho.morphosmart.sdk.MorphoImage;

public class FingerprintService {

   public void read(MorphoDevice device) {
      final MorphoImage[] morphoImage = new MorphoImage[] {new MorphoImage()};

      int compressRate = 0;
      int timeOut = 0;
      int acquisitionThreshold = 0;

      final int ret = device.getImage(timeOut, acquisitionThreshold, CompressionAlgorithm.MORPHO_NO_COMPRESS, compressRate, 18, LatentDetection.LATENT_DETECT_DISABLE, morphoImage[0], 195, null);
   }
}

package com.example.myapplication;

import android.Manifest;

import android.app.PendingIntent;
import android.app.ProgressDialog;

import android.content.BroadcastReceiver;
import android.content.Context;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;


import com.example.myapplication.database.FingerDBHelper;


import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.os.StrictMode;
import android.telecom.Call;
import android.util.Log;
import android.view.View;

import com.example.myapplication.databinding.ActivityMainBinding;

import com.morpho.android.usb.USBManager;
import com.morpho.morphosmart.sdk.CallbackMessage;
import com.morpho.morphosmart.sdk.MorphoDevice;
import com.morpho.morphosmart.sdk.MorphoImage;
import com.rscja.deviceapi.UsbFingerprint;

import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;


import java.nio.ByteBuffer;

import java.util.ArrayList;

import java.util.Observable;
import java.util.Observer;


public class MainActivity extends AppCompatActivity implements Observer  {

    private final String TAG = MainActivity.class.getSimpleName();

    private static final String ACTION_USB_PERMISSION = "com.example.USB_PERMISSION";

    private Handler			mHandler				= new Handler();

    private ActivityMainBinding binding;

    private MorphoDevice morphoDevice;



    private int FingerPrintImage = R.id.FingerPrintImage;

    private  ArrayList<Bitmap> images = new ArrayList<Bitmap>();



    private UsbDeviceConnection usbDeviceConnection = null;

    MorphoClass morpho;

    private UsbManager usbManager;

    private UsbDevice device;


    FingerDBHelper mDbHelper = null;

    static {
        try {
            System.loadLibrary("MSO_Secu");
        } catch (UnsatisfiedLinkError e) {
            Log.e("MorphoSample", "Exception in loadLibrary: " + e);
            e.printStackTrace();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        new InitTask().execute();

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        morpho = new MorphoClass();

        morphoDevice = new MorphoDevice();
        checkReadWritePermission();

        setSupportActionBar(binding.toolbar);

        mDbHelper = new FingerDBHelper(MainActivity.this);


    }

    public void initMorpho(View v){
        usbManager = (UsbManager) this.getSystemService(Context.USB_SERVICE);
        morpho.initMorpho(morphoDevice, TAG, usbManager,usbDeviceConnection, MainActivity.this);
    }

    public void readBiometric(View v){
        morpho.readBiometric(FingerPrintImage, morphoDevice, TAG, MainActivity.this);
    }

    public void validateBiometric (View v){
        morpho.validateBiometric(FingerPrintImage, morphoDevice, TAG, MainActivity.this);
    }

    public void getTemplate(View v){
        EditText userId = (EditText) findViewById(R.id.idCaptureInput);
        EditText userFirstName = (EditText) findViewById(R.id.nameCaptureInput);
        EditText userLastName = (EditText) findViewById(R.id.lastNameCaptureInput);

        morpho.getTemplate(userId, userFirstName, userLastName, FingerPrintImage, morphoDevice, MainActivity.this, mDbHelper);
    }

    public void deleteDataOnDatabase(View v) {
        morpho.deleteDataOnDatabase(mDbHelper, morphoDevice, MainActivity.this);
    }

    public void addUser(View v){
        morpho.addUser(mDbHelper, morphoDevice, MainActivity.this);
    }

    public void verifyUser(View v){
        morpho.verifyUser(morphoDevice, MainActivity.this);
    }

    public void readBiometricAndDatabase(View v) {
        EditText userId = (EditText) findViewById(R.id.getID);
        EditText userFirstName = (EditText) findViewById(R.id.getFirstName);
        EditText userLastName = (EditText) findViewById(R.id.getLastName);

        morpho.readBiometricAndDatabase(morphoDevice, TAG, FingerPrintImage, MainActivity.this, userId, userFirstName, userLastName);
    }

    public void loadFingerPrintInLoop(View v) {
            final Callback[] cbContainer = new Callback[1];
//            cbContainer[0] = new Callback() {
//                @Override
//                public void cb(int ret) {
//                    if (ret == 0) {
//                        morpho.readBiometric(FingerPrintImage, morphoDevice, TAG, MainActivity.this, cbContainer[0]);
//                    }
//                }
//            };
//
//
//           morpho.readBiometric(FingerPrintImage, morphoDevice, TAG, MainActivity.this, cbContainer[0]);

            Thread st = new Thread(() -> {
                if (cbContainer[0] != null) {
                    morpho.readBiometric(FingerPrintImage, morphoDevice, TAG, MainActivity.this, cbContainer[0]);
                }
            });

            cbContainer[0] = new Callback() {
                @Override
                public void cb(int ret) {
                    st.run();
                }
            };


            st.run();


    }

    public void onResume() {
        super.onResume();
    }

    @Override
    public synchronized void update(Observable o, Object arg) {
        try
        {
            // convert the object to a callback back message.
            CallbackMessage message = (CallbackMessage) arg;

            int type = message.getMessageType();

            switch (type)
            {

                case 1:
                    // message is a command.
                    Integer command = (Integer) message.getMessage();

                    // Analyze the command.
                    break;
                case 2:
                    // message is a low resolution image, display it.
                    byte[] image = (byte[]) message.getMessage();

                    MorphoImage morphoImage = MorphoImage.getMorphoImageFromLive(image);
                    int imageRowNumber = morphoImage.getMorphoImageHeader().getNbRow();
                    int imageColumnNumber = morphoImage.getMorphoImageHeader().getNbColumn();
                    final Bitmap imageBmp = Bitmap.createBitmap(imageColumnNumber, imageRowNumber, Bitmap.Config.ALPHA_8);

                    images.add(imageBmp);
                    imageBmp.copyPixelsFromBuffer(ByteBuffer.wrap(morphoImage.getImage(), 0, morphoImage.getImage().length));

                    if(imageBmp == null){
                        break;
                    }
                    mHandler.post(new Runnable()
                    {
                        @Override
                        public synchronized void run()
                        {
                            ImageView iv = (ImageView) findViewById(FingerPrintImage);
                            iv.setImageBitmap(imageBmp);
                        }
                    });
                    break;
                case 3:
                    // message is the coded image quality.
                    final Integer quality = (Integer) message.getMessage();
                    mHandler.post(new Runnable()
                    {
                        @Override
                        public synchronized void run()
                        {
                            ImageView iv = (ImageView) findViewById(FingerPrintImage);
                            Drawable drawable = null;
                            if(iv != null) {
                                iv.setBackground(drawable);
                            }
                        }
                    });
                    break;
                //case 4:
                //byte[] enrollcmd = (byte[]) message.getMessage();
            }
        }
        catch (Exception e)
        {
            Log.i(TAG, e.toString());
        }
    }

    public class InitTask extends AsyncTask<String, Integer, Boolean> {
        ProgressDialog mypDialog;

        @Override
        protected Boolean doInBackground(String... params) {

            usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);

            PendingIntent permissionIntent = PendingIntent.getBroadcast(MainActivity.this, 0, new Intent(ACTION_USB_PERMISSION), PendingIntent.FLAG_IMMUTABLE);
            IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
            registerReceiver(usbReceiver, filter);

            for (UsbDevice mDevice : usbManager.getDeviceList().values()) {
                device = mDevice;
                break;
            }

            if (device != null) {
                usbManager.requestPermission(device, permissionIntent);
            }

            UsbFingerprint.getInstance().FingerprintSwitchUsb();

            UsbFingerprint.getInstance().UsbToFingerprint();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            USBManager.getInstance().initialize(MainActivity.this, "com.example.myapplication.USB_ACTION", true);

            if(USBManager.getInstance().isDevicesHasPermission() == true)
            {
                Log.d(TAG, "Permission granted");
                return true;
            }

            Log.d(TAG, "Permission denied");

            return false;
        }
    }
    private void checkReadWritePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            }
            if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        }
    }

    public void startNewWindow(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


    private final BroadcastReceiver usbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if (device != null) {
                            // chame sua função para lidar com o dispositivo aqui
                        }
                    } else {
                        // trate a falha em conseguir a permissão
                    }
                }
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(usbReceiver);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        return super.onSupportNavigateUp();
        /*NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();*/
    }
}

interface Callback {
    void cb(int rt);
}
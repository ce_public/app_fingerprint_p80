package com.example.myapplication.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class FingerDBHelper extends SQLiteOpenHelper {
        private static final String TEXT_TYPE = " TEXT";
        private static final String COMMA_SEP = ",";
        private static final String SQL_CREATE_POSTS =
                "CREATE TABLE " + FingerContract.FingerEntry.TABLE_NAME + " (" +
                        FingerContract.FingerEntry._ID + " INTEGER PRIMARY KEY," +
                        FingerContract.FingerEntry.COLUMN_1_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                        FingerContract.FingerEntry.COLUMN_2_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                        FingerContract.FingerEntry.COLUMN_3_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                        FingerContract.FingerEntry.COLUMN_4_NAME_TITLE + TEXT_TYPE + " )";

        private static final String SQL_DELETE_POSTS =
                "DROP TABLE IF EXISTS " + FingerContract.FingerEntry.TABLE_NAME;

        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "FeedReader.db";

        public FingerDBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
         @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_POSTS);
        }
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(SQL_DELETE_POSTS);
            onCreate(db);
        }
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }
}

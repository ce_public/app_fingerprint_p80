package com.example.myapplication.database;

import android.provider.BaseColumns;

public class FingerContract {
    private FingerContract() {}

    public static class FingerEntry implements BaseColumns {
        public static final String TABLE_NAME = "fingers";
        public static final String COLUMN_1_NAME_TITLE = "id";
        public static final String COLUMN_2_NAME_TITLE = "Name";

        public static  final String COLUMN_3_NAME_TITLE = "LastName";
        public static  final String COLUMN_4_NAME_TITLE = "Template";

    }
}

package com.example.myapplication;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Environment;
import android.telecom.Call;
import android.util.Log;
import android.widget.EditText;

import com.example.myapplication.database.DatabaseItem;
import com.example.myapplication.database.FingerContract;
import com.example.myapplication.database.FingerDBHelper;
import com.machinezoo.sourceafis.FingerprintImage;
import com.machinezoo.sourceafis.FingerprintMatcher;
import com.machinezoo.sourceafis.FingerprintTemplate;
import com.morpho.morphosmart.sdk.Coder;
import com.morpho.morphosmart.sdk.CompressionAlgorithm;
import com.morpho.morphosmart.sdk.EnrollmentType;
import com.morpho.morphosmart.sdk.ErrorCodes;
import com.morpho.morphosmart.sdk.ITemplateType;
import com.morpho.morphosmart.sdk.LatentDetection;
import com.morpho.morphosmart.sdk.MatchingStrategy;
import com.morpho.morphosmart.sdk.MorphoDatabase;
import com.morpho.morphosmart.sdk.MorphoDevice;
import com.morpho.morphosmart.sdk.MorphoImage;
import com.morpho.morphosmart.sdk.MorphoTypeDeletion;
import com.morpho.morphosmart.sdk.MorphoUser;
import com.morpho.morphosmart.sdk.MorphoUserList;
import com.morpho.morphosmart.sdk.ResultMatching;
import com.morpho.morphosmart.sdk.StrategyAcquisitionMode;
import com.morpho.morphosmart.sdk.Template;
import com.morpho.morphosmart.sdk.TemplateFVP;
import com.morpho.morphosmart.sdk.TemplateFVPType;
import com.morpho.morphosmart.sdk.TemplateList;
import com.morpho.morphosmart.sdk.TemplateType;


import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class MorphoClass {


    final MorphoDatabase morphoDatabase = new MorphoDatabase();

    private List<DatabaseItem>			databaseItems						=  new ArrayList<DatabaseItem>();

    public String RootPath = Environment.getExternalStorageDirectory() + File.separator + "logs" + File.separator + "images" + File.separator;
    public void initMorpho(MorphoDevice morphoDevice, String TAG, UsbManager usbManager, UsbDeviceConnection usbDeviceConnection,  MainActivity mainActivity) {
        final String[] message = {""};

        Integer nbUsbDevice = new Integer(0);
        int result = morphoDevice.initUsbDevicesNameEnum(nbUsbDevice);
        String sensorName = morphoDevice.getUsbDeviceName(0);

        int ret = morphoDevice.openUsbDevice(sensorName, 0);
        Log.i("MORPHO_USB", "Opening device in DeviceDetectionMode.SdkDetection returns " + ret);

        if (result == ErrorCodes.MORPHO_OK) {
            ret = morphoDevice.getDatabase(0, new MorphoDatabase());
            Log.i("MORPHO_USB", "morphoDevice.getDatabase = " + ret);
            Log.d(TAG, "Comunicação realizada com sucesso");

            System.out.println("Comunicação realizada com sucesso");

            if (nbUsbDevice > 0) {
                sensorName = morphoDevice.getUsbDeviceName(0);
                Log.d(TAG, "Nome do sensor" + sensorName);
                openDevice(-1, -1, -1,usbDeviceConnection, morphoDevice, usbManager);

            }
            message[0] = "Morpho Inicializado com sucesso";
        } else {
            message[0] = "Erro ao inicializar";
        }

        Log.d(TAG, "Comunicação não pôde ser realizada" + result);

        alert(ret, "Identify", message[0], true, mainActivity);
    }


    public int openDevice(int bus, int address, int fd, UsbDeviceConnection usbDeviceConnection, MorphoDevice morphoDevice,  UsbManager usbManager) {
        int ret = ErrorCodes.MORPHO_OK;

        HashMap<String, UsbDevice> usbDeviceList = usbManager.getDeviceList();

        Iterator<UsbDevice> usbDeviceIterator = usbDeviceList.values().iterator();
        while (usbDeviceIterator.hasNext()) {
            UsbDevice usbDevice = usbDeviceIterator.next();
            if (MorphoTools.isSupported(usbDevice.getVendorId(), usbDevice.getProductId())) {
                boolean hasPermission = usbManager.hasPermission(usbDevice);
                if (hasPermission) {
                    usbDeviceConnection = usbManager.openDevice(usbDevice);
                    if (usbDeviceConnection != null)
                    {
                        //Log.d("MORPHO_USB" ,"getting serial number .. #" + mConnection.getSerial());
                        // int sensorFileDescriptor = connection.getFileDescriptor();
                        String name = usbDevice.getDeviceName();

                        String[] elts = name.split("/");
                        if(elts.length < 5)
                            continue;
                        int sensorBus = Integer.parseInt(elts[4].toString());
                        int sensorAddress = Integer.parseInt(elts[5].toString());
                        //device fd can change on device reboot
                        if(sensorBus == bus && sensorAddress == address /*&& sensorFileDescriptor == fd*/) {
                            ret = morphoDevice.openUsbDeviceFD(bus, address, fd, 0);
                            //Log.e("MORPHO_USB", "morphoDevice.openUsbDeviceFD " + ret);
                        }
                        break;
                    }
                }
            }
        }

        return ret;
    }

    public void readBiometric(int FingerPrintImage, MorphoDevice morphoDevice, String TAG, MainActivity mainActivity) {
        final MorphoImage[] morphoImage = new MorphoImage[] {new MorphoImage()};
        String message = "";

        int compressRate = 100;
        int timeOut = 10;
        int acquisitionThreshold = 0;
        FingerPrintImage = R.id.FingerPrintImage;

        String fileName = RootPath+"_"+new Random().nextInt(100) + ".wsq";

        final int ret = morphoDevice.getImage(timeOut, acquisitionThreshold, CompressionAlgorithm.MORPHO_COMPRESS_WSQ, compressRate, 18, LatentDetection.LATENT_DETECT_DISABLE, morphoImage[0], 195, mainActivity);

        if(ret == ErrorCodes.MORPHO_OK) {
            byte[] bytes = morphoImage[0].getCompressedImage();
            Log.d(TAG, "Fingerprint coletada com sucesso" + bytes);
            try {
                FileOutputStream fos = null;
                fos = new FileOutputStream(fileName);
                message = "Image RAW successfully exported in file [" + fileName + "]";
                fos.write(bytes);
                fos.close();

                Subject subject = new Subject(new Random().nextInt(100), "Mateus", bytes);
                saveDB(subject, RootPath);
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        }
        alert(ret, "Capture", message, true, mainActivity);
    }

    public void readBiometric(int FingerPrintImage, MorphoDevice morphoDevice, String TAG, MainActivity mainActivity, Callback cb) {
        final MorphoImage[] morphoImage = new MorphoImage[] {new MorphoImage()};
        String message = "";
        int ret = 0;

        int compressRate = 100;
        int timeOut = 10;
        int acquisitionThreshold = 0;
        FingerPrintImage = R.id.FingerPrintImage;

        String fileName = RootPath+"_"+new Random().nextInt(100) + ".wsq";
//        boolean isReading = true;
//
//        while(isReading) {
            ret = morphoDevice.getImage(timeOut, acquisitionThreshold, CompressionAlgorithm.MORPHO_COMPRESS_WSQ, compressRate, 18, LatentDetection.LATENT_DETECT_DISABLE, morphoImage[0], 195, mainActivity);
            if (ret == ErrorCodes.MORPHO_OK) {
                byte[] bytes = morphoImage[0].getCompressedImage();
                Log.d(TAG, "Fingerprint coletada com sucesso" + bytes);
                try {
                    FileOutputStream fos = null;
                    fos = new FileOutputStream(fileName);
                    message = "Image RAW successfully exported in file [" + fileName + "]";
                    fos.write(bytes);
                    fos.close();

                    Subject subject = new Subject(new Random().nextInt(100), "Mateus", bytes);
                    saveDB(subject, RootPath);
                } catch (FileNotFoundException e) {
                    throw new RuntimeException(e);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

//               isReading = false;
//            }
        }
        final int retCallback = ret;
            if(ret == ErrorCodes.MORPHO_OK){
                alert(ret, "Capture", message, true, mainActivity, () ->  cb.cb(retCallback));
                return;
            }
            cb.cb(ret);

    }


    public void validateBiometric (int FingerPrintImage, MorphoDevice morphoDevice, String TAG, MainActivity mainActivity) {
        try {

            final MorphoImage[] morphoImage = new MorphoImage[] {new MorphoImage()};
            String message = "";

            int compressRate = 100;
            int timeOut = 10;
            int acquisitionThreshold = 0;
            FingerPrintImage = R.id.FingerPrintImage;


            final int ret = morphoDevice.getImage(timeOut, acquisitionThreshold, CompressionAlgorithm.MORPHO_COMPRESS_WSQ, compressRate, 18, LatentDetection.LATENT_DETECT_DISABLE, morphoImage[0], 195, mainActivity);
            if(ret == ErrorCodes.MORPHO_OK) {
                byte[] bytes = morphoImage[0].getCompressedImage();
                FingerprintTemplate probe = new FingerprintTemplate(
                        new FingerprintImage(bytes));
                FingerprintTemplate candidate = new FingerprintTemplate(
                        new FingerprintImage(Files.readAllBytes(Paths.get(RootPath + "_" + "61.wsq"))));
                FingerprintMatcher matcher = new FingerprintMatcher(probe);
                double similarity = matcher.match(candidate);
                Log.d(TAG, "Fingerprint verificada com sucesso " + similarity);

            }

        }catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public void saveDB(Subject candidates, String folderPath) {
        // Cria a pasta se não existir
        File folder = new File(folderPath);
        if (!folder.exists()) {
            folder.mkdirs();
        }

        // Itera sobre os candidatos e salva cada um em um arquivo na pasta

        // Cria um nome de arquivo único para cada candidato
        String filename ="subject_" + new Random().nextInt(100) + ".dat";
        File file = new File(folder, filename);

        // Cria um fluxo de saída de objeto para escrever o objeto do candidato no arquivo
        FileOutputStream fileOut = null;
        try {
            fileOut = new FileOutputStream(file);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);

            // Escreve o objeto do candidato no arquivo
            objectOut.writeObject(candidates);

            // Fecha os fluxos
            objectOut.close();
            fileOut.close();
        } catch (FileNotFoundException ex) {
            throw new RuntimeException(ex);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }


        System.out.println("Candidato salvo com sucesso: " + file.getAbsolutePath());


    }

    public static List<Subject> loadDB(String folderPath) {
        List<Subject> candidates = new ArrayList<>();

        // Abre a pasta especificada
        File folder = new File(folderPath);

        // Verifica se a pasta existe e é um diretório
        if (folder.exists() && folder.isDirectory()) {
            // Lista todos os arquivos na pasta
            File[] files = folder.listFiles();

            // Itera sobre os arquivos
            for (File file : files) {
                try {
                    // Lê o objeto do arquivo
                    FileInputStream fileIn = new FileInputStream(file);
                    ObjectInputStream objectIn = new ObjectInputStream(fileIn);
                    Subject subject = (Subject) objectIn.readObject();
                    objectIn.close();
                    fileIn.close();

                    // Adiciona o objeto lido à lista de candidatos
                    candidates.add(subject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            System.out.println("A pasta especificada não existe ou não é um diretório.");
        }

        return candidates;
    }

    public void getTemplate(EditText userId, EditText userFirstName, EditText userLastName, int FingerPrintImage, MorphoDevice morphoDevice, MainActivity mainActivity, FingerDBHelper mDbHelper){
        final TemplateList templateList = new TemplateList();
        int timeout = 0;
        int acquisitionThreshold = 0;
        int advancedSecurityLevelsRequired = 0;

        int nbFinger = 1;

        String	idUser = userId.getText().toString();
        String nameUser = userFirstName.getText().toString();
        String lastNameUser = userLastName.getText().toString();

        TemplateType templateType = TemplateType.MORPHO_PK_COMP;
        TemplateFVPType templateFVPType = TemplateFVPType.MORPHO_NO_PK_FVP;
        int maxSizeTemplate = 255;
        EnrollmentType enrollType = EnrollmentType.THREE_ACQUISITIONS;
        LatentDetection latentDetection = LatentDetection.LATENT_DETECT_DISABLE;
        Coder coderChoice = Coder.MORPHO_DEFAULT_CODER;
        int detectModeChoice = 18;
        int callbackCmd = 199;

        FingerPrintImage = R.id.fingerCaptureImage;

        int res = morphoDevice.setStrategyAcquisitionMode(StrategyAcquisitionMode.MORPHO_ACQ_EXPERT_MODE);

        int ret = morphoDevice.capture(timeout, acquisitionThreshold, advancedSecurityLevelsRequired,
                nbFinger, templateType, templateFVPType, maxSizeTemplate, enrollType,
                latentDetection, coderChoice, detectModeChoice, CompressionAlgorithm.MORPHO_NO_COMPRESS, 0, templateList, callbackCmd, mainActivity);
        String message = "";
        if (ret == ErrorCodes.MORPHO_OK) {
            final int NbTemplate = 1;
            final TemplateType finalTemplateType = templateType;

            for (int i = 0; i < NbTemplate; i++) {
                Template t = templateList.getTemplate(i);
                message += "Finger #" + (i + 1) + " - Quality Score: " + t.getTemplateQuality() + "\n";
            }

            for (int i = 0; i < NbTemplate; i++) {
                try {
                    String file_name = RootPath + "templates" +  File.separator + "TemplateFP_" + idUser;

                    file_name += templateType.getExtension();
                    Template t = templateList.getTemplate(i);
                    FileOutputStream fos = new FileOutputStream(file_name);
                    fos.write(t.getData());
                    fos.close();

                    AddTemplateInDatabase(idUser, nameUser, lastNameUser, file_name, mDbHelper);
                    //addOnMorphoDB(idUser, file_name , nameUser, lastNameUser);
                    message += "Finger #" + (i + 1) + " - FP Template successfully exported in file [" + file_name + "]\n";
                } catch(FileNotFoundException e) {
                    Log.i("CAPTURE", e.getMessage());
                } catch (IOException e)	{
                    Log.i("CAPTURE", e.getMessage());
                }
            }
        } else {
            message = "Falha ao Capturar Impressão Digital";
        }
        alert(ret, "Capture", message, true, mainActivity);
    }


    public void AddTemplateInDatabase(String id, String name, String lastName, String template, FingerDBHelper mDbHelper){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(FingerContract.FingerEntry.COLUMN_1_NAME_TITLE, id);
        values.put(FingerContract.FingerEntry.COLUMN_2_NAME_TITLE, name);
        values.put(FingerContract.FingerEntry.COLUMN_3_NAME_TITLE, lastName);
        values.put(FingerContract.FingerEntry.COLUMN_4_NAME_TITLE, template);

        long newRowId = db.insert(FingerContract.FingerEntry.TABLE_NAME, null, values);
    }

    public List<Fingerprints> readTemplateInDatabase(FingerDBHelper mDbHelper){
        List list = new ArrayList<Fingerprints>();
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = {
                FingerContract.FingerEntry._ID,
                FingerContract.FingerEntry.COLUMN_1_NAME_TITLE,
                FingerContract.FingerEntry.COLUMN_2_NAME_TITLE,
                FingerContract.FingerEntry.COLUMN_3_NAME_TITLE
        };

        String selection = FingerContract.FingerEntry.COLUMN_1_NAME_TITLE + " = ?";
        String[] selectionArgs = {};

        String sortOrder =
                FingerContract.FingerEntry.COLUMN_1_NAME_TITLE + " DESC";

        Cursor c = db.query(
                FingerContract.FingerEntry.TABLE_NAME,
                null,
                null,
                selectionArgs,
                null,
                null,
                sortOrder);

        c.moveToFirst();
        while(!c.isAfterLast()){
            Fingerprints fingers = fillFingers(c);
            list.add(fingers);
            c.moveToNext();
        }
        c.close();
        db.close();
        return list;
    }

    private Fingerprints fillFingers(Cursor c) {
        Fingerprints fingers = new Fingerprints();
        fingers.setId(c.getString(1));
        fingers.setName(c.getString(2));
        fingers.setLastName(c.getString(3));
        fingers.setTemplate(c.getString(4));
        return fingers;
    }

    public void deleteDataOnDatabase(FingerDBHelper mDbHelper, MorphoDevice morphoDevice,  MainActivity mainActivity) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        int rows = db.delete(FingerContract.FingerEntry.TABLE_NAME, null,
                null);

        removeFiles();

        int ret = morphoDevice.getDatabase(0, morphoDatabase);
        ret = morphoDatabase.dbDelete(MorphoTypeDeletion.MORPHO_ERASE_BASE);

        if(ret == ErrorCodes.MORPHO_OK){
            alert(0, "Delete DB", "Linhas afetadas: " + rows, true, mainActivity);
        } else {
            alert(0, "Delete DB", "Falha ao deletar o banco dedos", true, mainActivity);
        }

    }

    public void removeFiles() {
        File f = new File(RootPath + "templates");
        if (f.isDirectory()) {

            File[] files = f.listFiles();
            for (File file : files) {
                file.delete();
            }
        }
    }

    public String addOnMorphoDB(String id, String path, String name, String lastName, MorphoDevice morphoDevice) {

        int ret = 0;
        String additionalMessage = "";

        try
        {
            String fileName = path;
            DataInputStream dis = new DataInputStream(new FileInputStream(fileName));
            int length = dis.available();
            final byte[] buffer = new byte[length];
            dis.readFully(buffer);
            dis.close();
            final Template template = new Template();
            final TemplateFVP templateFVP = new TemplateFVP();

            ret = morphoDevice.getDatabase(0, morphoDatabase);


            final ITemplateType iTemplateType = TemplateType.MORPHO_PK_COMP;

            if (iTemplateType != TemplateType.MORPHO_NO_PK_FP) {
                if (iTemplateType instanceof TemplateFVPType) {
                    templateFVP.setTemplateFVPType((TemplateFVPType) iTemplateType);
                } else {
                    template.setTemplateType((TemplateType) iTemplateType);
                }
            } else {
                return fileName + " not valid !!";
            }


            byte[] random = new byte[4];
            new Random().nextBytes(random);
            MorphoUser morphoUser = new MorphoUser();


            ret = morphoDatabase.getUser(id, morphoUser);


            if (ret == 0) {
                ret = morphoUser.putField(1, MorphoTools.checkfield(name, false));
            }

            if (ret == 0) {
                ret = morphoUser.putField(2, MorphoTools.checkfield(lastName, false));
            }

            if (ret == 0) {
                Integer index = new Integer(0);

                if (iTemplateType instanceof TemplateType) {
                    template.setData(buffer);
                    morphoUser.putTemplate(template, index);
                } else {
                    templateFVP.setData(buffer);
                    morphoUser.putFVPTemplate(templateFVP, index);
                }
            }

            ret = morphoUser.dbStore();
            if (ret == 0) {
                DatabaseItem databaseItemsItem = new DatabaseItem(id, MorphoTools.checkfield(name, false), MorphoTools.checkfield(lastName, false));
                List<DatabaseItem> datbaseItems = databaseItems;
                datbaseItems.add(databaseItemsItem);
                loadDatabaseItem();
                additionalMessage += "Usuário " + name + " " + lastName + "(" + id + ")" + " adicionado com sucesso \n";
            } else if (ret == ErrorCodes.MORPHOERR_INVALID_USER_ID || ret == ErrorCodes.MORPHOERR_ALREADY_ENROLLED) {
                additionalMessage += "Usuário"  + name + " " + lastName + "(" + id + ")" + " já cadastrado \n";
            } else {
                additionalMessage += "Falha ao adicionar o usuário "  + name + " " + lastName + "(" + id + ")" +"\n";
            }
        } catch (FileNotFoundException e)
        {
            additionalMessage += "O template do usuário " + name + " " + lastName + "(" + id + ")" + " Não foi encontrado\n";
        }
        catch (IOException e)
        {
            additionalMessage += "Erro ao adicionar o usuário " +  name + " " + lastName + "(" + id + ")" +"\n";
        }
        return additionalMessage;
    }

    public void addUser(FingerDBHelper mDbHelper, MorphoDevice morphoDevice,  MainActivity mainActivity)
    {

        int ret = 0;
        String additionalMessage = "";
        List<Fingerprints> list = readTemplateInDatabase(mDbHelper);
        for(Fingerprints finger : list){
            additionalMessage +=  addOnMorphoDB(finger.getId(), finger.getTemplate(), finger.getName(), finger.getLastName(), morphoDevice);
        }
        alert(ret, "Success", additionalMessage, true, mainActivity);

    }

    public int loadDatabaseItem()
    {
        int ret = 0;
        databaseItems = new ArrayList<DatabaseItem>();
        int[] indexDescriptor = new int[3];
        indexDescriptor[0] = 0;
        indexDescriptor[1] = 1;
        indexDescriptor[2] = 2;

        MorphoUserList morphoUserList = new MorphoUserList();
        ret = morphoDatabase.readPublicFields(indexDescriptor, morphoUserList);

        int l_nb_user = morphoUserList.getNbUser();
        for (int i = 0; i < l_nb_user; i++)
        {
            MorphoUser morphoUser = morphoUserList.getUser(i);


            String userID = morphoUser.getField(0);
            String firstName = morphoUser.getField(1);
            String lastName = morphoUser.getField(2);
            databaseItems.add(new DatabaseItem(userID, firstName, lastName));

        }

        return ret;
    }

    public void verifyUser(MorphoDevice morphoDevice, MainActivity mainActivity){
        final String[] message = {""};
        int timeout = 300;
        int far = 5;
        Coder coder = Coder.MORPHO_DEFAULT_CODER;
        int detectModeChoice = 18;
        MatchingStrategy matchingStrategy = MatchingStrategy.MORPHO_STANDARD_MATCHING_STRATEGY;
        int callbackCmd = 195;
        ResultMatching resultMatching = new ResultMatching();
        final MorphoUser morphoUser = new MorphoUser();
        int ret = morphoDevice.setStrategyAcquisitionMode(StrategyAcquisitionMode.MORPHO_ACQ_EXPERT_MODE);
        boolean isTunnelingMode = false;
        boolean isOfferedSecurityMode = false;
        ret = morphoDevice.getDatabase(0, morphoDatabase);

        if(ret == 0) {
            ret = morphoDatabase.identify(timeout, far, coder, detectModeChoice, matchingStrategy, callbackCmd, mainActivity, resultMatching, 2, morphoUser);
        }

        if (ret == 0) {
            switch (ret) {
                case 8:
                    message[0] = "Nnehum usuário encontrado";
                    break;
                case -16:
                    message[0] = "Nenhum dispositivo conectado";
                    break;
                case 0:
                    String userID = morphoUser.getField(0);
                    String firstName = morphoUser.getField(1);
                    String lastName = morphoUser.getField(2);
                    message[0] = "User identified";
                    message[0] += "\r\nUser ID   : \t\t" + userID;
                    message[0] += "\r\nFirstName : \t\t" + firstName;
                    message[0] += "\r\nLastName  : \t\t" + lastName;
                    break;

                default:
                    message[0] = "Erro ao verificar o usuário";
                    break;
            }
        }

        alert(ret, "Identify", message[0], true, mainActivity);

    }

    public void readBiometricAndDatabase(MorphoDevice morphoDevice, String TAG, int FingerPrintImage, MainActivity mainActivity, EditText userId, EditText userFirstName, EditText userLastName) {
        try {
            MorphoUser morphoUser = new MorphoUser();
            int ret = 0;

            final String[] message = {""};


            String	idUser = userId.getText().toString();
            String	firstName = userFirstName.getText().toString();
            String	lastName = userLastName.getText().toString();

            ret = morphoDevice.getDatabase(0, morphoDatabase);
            ret = morphoDatabase.getUser(idUser, morphoUser);
            if (ErrorCodes.MORPHO_OK == ret) {
                ret = morphoUser.putField(1, MorphoTools.checkfield(firstName,false));
                if (ErrorCodes.MORPHO_OK == ret) {
                    ret = morphoUser.putField(2, MorphoTools.checkfield(lastName, false));
                }
            }
            int timeout = 100;

            int acquisitionThreshold = 0;
            int advancedSecurityLevelsRequired = 0;
            final CompressionAlgorithm compressAlgo = CompressionAlgorithm.NO_IMAGE;
            int compressRate = 0;
            int exportMinutiae = 1;
            final TemplateType templateType = TemplateType.MORPHO_NO_PK_FP;
            TemplateFVPType templateFVPType = TemplateFVPType.MORPHO_NO_PK_FVP;
            Coder coder = Coder.MORPHO_DEFAULT_CODER;
            final int fingerNumber = 1;
            boolean saveRecord = true;
            int detectModeChoice = 18;
            TemplateList templateList = new TemplateList();
            templateList.setActivateFullImageRetrieving(true);
            int callbackCmd= 199;


            ret = morphoDevice.setStrategyAcquisitionMode(StrategyAcquisitionMode.MORPHO_ACQ_EXPERT_MODE);

            FingerPrintImage = R.id.IdentifyImage;
            ret = morphoUser.enroll(
                    timeout,
                    acquisitionThreshold,
                    advancedSecurityLevelsRequired,
                    compressAlgo,
                    compressRate,
                    exportMinutiae,
                    fingerNumber,
                    templateType,
                    templateFVPType,
                    saveRecord,
                    coder,
                    detectModeChoice,
                    templateList,
                    callbackCmd,
                    mainActivity
            );


            if (ErrorCodes.MORPHO_OK == ret && saveRecord) {
                DatabaseItem databaseItemsItem = new DatabaseItem(idUser, firstName, lastName);
                List<DatabaseItem> datbaseItems = databaseItems;
                datbaseItems.add(databaseItemsItem);
                databaseItems = datbaseItems;
                message[0] = "Usuário adicionado com sucesso";
            } else if(ErrorCodes.MORPHOERR_ALREADY_ENROLLED == ret) {
                message[0] = "Usuário já adicionado";
            } else {
                message[0] = "Erro ao adicionar o usuário";
            };
            alert(ret, "Add User DB", message[0], true, mainActivity);

        } catch (Exception e) {
            Log.i(TAG, e.toString());
        }
    }

    public void alert(int codeError, String title, String message, boolean isMatchingOperation, MainActivity mainActivity, Runnable onOkClicked)
    {
        AlertDialog alertDialog = new AlertDialog.Builder(mainActivity).create();
        alertDialog.setTitle(title);
        String msg;
        if (codeError == 0)
        {
            msg = "Operação realizada com sucesso" + "\n" + message;
        }
        else
        {

            msg = "Falha ao realizar a operação" + "\n" + message;
        }
        alertDialog.setMessage(msg);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
                if (onOkClicked != null) {
                    onOkClicked.run();
                }
            }
        });
        alertDialog.show();
    }

    public void alert(int codeError, String title, String message, boolean isMatchingOperation, MainActivity mainActivity)
    {
        AlertDialog alertDialog = new AlertDialog.Builder(mainActivity).create();
        alertDialog.setTitle(title);
        String msg;
        if (codeError == 0)
        {
            msg = "Operação realizada com sucesso" + "\n" + message;
        }
        else
        {

            msg = "Falha ao realizar a operação" + "\n" + message;
        }
        alertDialog.setMessage(msg);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
            }
        });
        alertDialog.show();
    }

}
